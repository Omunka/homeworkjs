const pictures = document.querySelectorAll(".image")
let currentPictures = 0;
const stop = document.getElementById("stop").addEventListener("click", pause);
const start = document.getElementById("start").addEventListener("click", play);

let showPictures = setInterval(slider, 3000);
let showing = true;

function slider() {
    pictures[currentPictures].className = "image";
    currentPictures = (currentPictures + 1) % pictures.length;
    pictures[currentPictures].className = "image-to-show";
}

function pause() {
    clearInterval(showPictures);
    showing = false;
}

function play() {
    if (!showing) {
        showPictures = setInterval(slider, 3000);
        showing = true;
    }
}

