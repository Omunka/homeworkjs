#Теоретический вопрос

1. Опишите своими словами разницу между функциями `setTimeout()` и `setInterval()`.

setTimeout() - можем вызвать функцию один раз через определенное время
setInterval() - можем вызывать функцию какое-то количество раз через определенные интервалы 

2. Что произойдет, если в функцию `setTimeout()` передать нулевую задержку? Сработает ли она мгновенно, и почему?

это планирует вызов функции как только это возмозно, но планировщик будет вызывать функцию только после завершения выполнения текущего кода. Так вызов функции будет запланирован сразу после выполнения текущего кода. 

3. Почему важно не забывать вызывать функцию `clearInterval()`, когда ранее созданный цикл запуска вам уже не нужен?

Потому что функция ссылается на внешнее лексическое окружение, поэтому пока она существует, внешние переменные существуют тоже. Они могут занимать больше памяти, чем сама функция.

## Задание

Реализовать программу, показывающую циклично разные картинки. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

#### Технические требования:
- В папке `banners` лежит HTML код и папка с картинками.
- При запуске программы на экране должна отображаться первая картинка.
- Через 3 секунды вместо нее должна быть показана вторая картинка.
- Еще через 3 секунды - третья.
- Еще через 3 секунды - четвертая.
- После того, как покажутся все картинки - этот цикл должен начаться заново.
- При запуске программы где-то на экране должна появиться кнопка с надписью `Прекратить`.
- По нажатию на кнопку `Прекратить` цикл завершается, на экране остается показанной та картинка, которая была там при нажатии кнопки.
- Рядом с кнопкой `Прекратить` должна быть кнопка `Возобновить показ`, при нажатии которой цикл продолжается с той картинки, которая в данный момент показана на экране.
- Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.

#### Необязательное задание продвинутой сложности:
- При запуске программы на экране должен быть таймер с секундами и миллисекундами, показывающий сколько осталось до показа следующей картинки.
- Делать скрытие картинки и показывание новой картинки постепенным (анимация fadeOut / fadeIn) в течение 0.5 секунды.

#### Литература:
- [setTimeout и setInterval](https://learn.javascript.ru/settimeout-setinterval)
