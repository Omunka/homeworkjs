
const arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
function listFromArr(arr) {
    const listItem = arr.map((elem) =>{
         return `<li>${elem}</li>`
        }).join('');
        return listItem;
}

let ul = document.createElement('ul');
document.body.append(ul);
ul.insertAdjacentHTML('afterbegin', `${listFromArr(arr)}`);
