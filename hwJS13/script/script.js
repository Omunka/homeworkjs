
const button = document.querySelector('.buttonChangeTheme');
const Theme = document.getElementById('light-theme');

window.onload = () => {
    if(localStorage.getItem('theme') === 'theme-dark'){
        Theme.innerHTML = '<link rel="stylesheet" href="css/styleDark.css" />'
    }else{
        Theme.innerHTML = '<link rel="stylesheet" href="css/style.css" />'
    }
}

button.addEventListener('click', function() {
    if (localStorage.getItem('theme') === 'theme-dark') {
        Theme.innerHTML = '<link rel="stylesheet" href="css/style.css" />'
        localStorage.setItem('theme', 'theme-light')
    }
    else {
        Theme.innerHTML = '<link rel="stylesheet" href="css/styleDark.css" />'
        localStorage.setItem('theme', 'theme-dark')
    }
})
