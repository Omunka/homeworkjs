function ActiveElem(event){
    for (let i = 0; i < tabItemsArray.length; i++) {
        if(tabItemsArray[i] === event.target){
            event.target.classList.add("active");
            tabsContentArray[i].classList.add("tab-content-active");
        }
        else{
            tabItemsArray[i].classList.remove("active");
            tabsContentArray[i].classList.remove("tab-content-active");
        }
    }
}

const tabItemsArray = document.querySelectorAll(".tabs-title");
const tabsContentArray = document.querySelectorAll(".tab-content-item");
document.querySelector(".tabs").addEventListener("click", ActiveElem);
