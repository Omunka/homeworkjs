function createNewUser (firstName, lastName, birthday) {
    return{
        firstName,
        lastName,
        birthday,
        getLogin() {
            return (`Ваш логин: ${this.firstName[0].toLowerCase()}${this.lastName.toLowerCase()}`);
        },
 
        getAge() {
            const today = new Date();
            const userBirthday = Date.parse(`${this.birthday.slice(6)}-${this.birthday.slice(3, 5)}-${this.birthday.slice(0, 2)}`);
            const age = ((today - userBirthday) / (1000 * 60 * 60 * 24 * 30 * 12)).toFixed(0);
            if (age < today) {
                return `Ваш возраст: ${age - 1}`;
            } else {
                return `Ваш возраст: ${age}`;
            }
        },
    
        getPassword() {
            return (`Ваш пароль: ${this.firstName[0].toUpperCase()}${this.lastName.toLowerCase()}${this.birthday.slice(6)}`); 
        },
    }
}

const newUser = createNewUser(prompt("Введите Ваше имя"), prompt("Введите Вашу фамилию"), prompt("Введите Вашу дату рождения (текст в формате dd.mm.yyyy)"));

console.log(newUser.getLogin(), newUser.getAge(), newUser.getPassword());  

