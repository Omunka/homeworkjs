const buttons = document.querySelectorAll('.btn');

document.addEventListener('keydown' , function(event){
    let target = event.key;
    buttons.forEach(function(el){
        if (el.dataset.btn === target){
            el.classList.add('active')
        }else{
            el.classList.remove('active')
        }
    })
})