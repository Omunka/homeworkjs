
$(".header-nav-menu").on("click", "a", function (event) {
    event.preventDefault(); //удаляем дефолтное поведения ссылки
    const id = $(this).attr("href"); // получит атрибут href внутри текущего нажатого элемента (this).
    const top = $(id).offset().top; // получаем верх нужного елемента (id)
    $("body, html").animate( //  анимация для всего документа, скролим до "const top"
        {
            scrollTop: top,
        },
        2000 // за 2с
    );
});

$('.up-button').click(function () {
    $("html, body").animate({ scrollTop: 0 }, 2000);
    return false;
});

$(window).scroll(function () {
    if ($(this).scrollTop() >= screen.height) {
        $(".up-button").fadeIn(); //показать
    } else {
        $(".up-button").fadeOut(); //скрить
    }
});


function slideToggle(section) {
    section.toggleClass("hidden-section");
}

$(".toggle-btn").on("click", () => {
    slideToggle($(".clients-grid"));
});
