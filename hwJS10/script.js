function showPassword(icon){
    const input = icon.closest("label").querySelector("input");
    if(icon.classList.contains("fa-eye-slash")){
        icon.classList.remove("fa-eye-slash");
        input.setAttribute("type", "password");
    }
    else{
        icon.classList.add("fa-eye-slash");
        input.setAttribute("type", "text");
    }
}

const input = document.getElementById('input');
const inputAgain = document.getElementById('inputAgain');
const form = document.querySelector('.password-form')

form.addEventListener('click', (event) => showPassword(event.target))

document.querySelector(".btn").addEventListener("click", () => {
    if(input.value === inputAgain.value){
        document.getElementById("validation-error").innerText = "";
        alert("You are welcome");
    }
    else{
        document.getElementById("validation-error").innerText = "Нужно ввести одинаковые значения";
    }
});
